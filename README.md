EXCEPTION-SERVICE

exception-service is a very lightweight microservice designed to collect business exceptions from various applications in a common service. The application exposes an HTTP rest endpoint at port 8888 where exceptions can be created or viewed from. Exceptions are stored in a backend Mongodb.

This application is currently running on gcloud at http://35.204.46.63:8888/exceptions/

Curl commands below to view list of all exceptions

-----------------------------------------------------------------------------------------
$ curl -X GET http://35.204.46.63:8888/exceptions/

[
    {
        "id": 1,
        "name": "Exception",
        "description": "Real Exception",
        "source": "metadata-service",
        "level": 1
    },
    {
        "id": 2,
        "name": "Billing Exception",
        "description": "Billing Exception. Product LW182 Pricing Configuration incorrect.",
        "source": "billing-service",
        "level": 5
    },
    {
        "id": 3,
        "name": "Interface Mapping Exception",
        "description": "Industry File not adhering to xml schema.",
        "source": "market-messaging-service",
        "level": 3
    }
]
-----------------------------------------------------------------------------------------
 
Curl POST command to create a new exception 

-----------------------------------------------------------------------------------------

curl -X POST http://35.204.46.63:8888/exceptions/ \
  -H 'content-type: application/json' \
  -d '{
		"id": 3,
        "name": "Interface Mapping Exception",
        "description": "Industry File not adhering to xml schema.",
        "source": "market-messaging-service",
        "level": 3
      }'

Response: 

{
    "id": 3,
    "name": "Interface Mapping Exception",
    "description": "Industry File not adhering to xml schema.",
    "source": "market-messaging-service",
    "level": 3
}

-----------------------------------------------------------------------------------------
