package com.padippurackal.integration.exception.exceptionservice.repository;

import com.padippurackal.integration.exception.exceptionservice.model.ExceptionData;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ExceptionRepository extends MongoRepository<ExceptionData, Long> {

}
