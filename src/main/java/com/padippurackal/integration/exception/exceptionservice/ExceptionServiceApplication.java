package com.padippurackal.integration.exception.exceptionservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@SpringBootApplication
@EntityScan("com.padippurackal.integration.exception.exceptionservice.model")
public class ExceptionServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExceptionServiceApplication.class, args);
	}
}
