package com.padippurackal.integration.exception.exceptionservice.service;

import com.padippurackal.integration.exception.exceptionservice.model.ExceptionData;
import com.padippurackal.integration.exception.exceptionservice.repository.ExceptionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ExceptionService {

    @Autowired
    private ExceptionRepository exceptionRepository;

    public List<ExceptionData> findAll(){
        return exceptionRepository.findAll();
    }

    public ExceptionData addExceptionResource(ExceptionData exceptionData) {
        return exceptionRepository.save(exceptionData);
    }
}
