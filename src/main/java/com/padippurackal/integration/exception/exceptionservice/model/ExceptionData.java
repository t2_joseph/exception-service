package com.padippurackal.integration.exception.exceptionservice.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;

@Document(collection = "exceptions")
@Data
public class ExceptionData {

    @Id
    private long id;

    @NotNull
    private String name;

    @NotNull
    private String description;

    @NotNull
    private String source;

    @NotNull
    private int level;
}
