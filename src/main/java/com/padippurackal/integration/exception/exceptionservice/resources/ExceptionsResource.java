package com.padippurackal.integration.exception.exceptionservice.resources;

import com.padippurackal.integration.exception.exceptionservice.model.ExceptionData;
import com.padippurackal.integration.exception.exceptionservice.service.ExceptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ExceptionsResource {

    @Autowired
    ExceptionService exceptionService;

    @GetMapping("/exceptions")
    public List<ExceptionData> getExceptions(){

        return exceptionService.findAll();
    }

    @PostMapping("/exceptions")
    public ExceptionData addException(@RequestBody ExceptionData exceptionData){

        return exceptionService.addExceptionResource(exceptionData);
    }

}

