#!/bin/bash

set -eu

SCRIPT_DIR=$(cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd)

REGISTRY_URL=dockerregistry
IMAGE_NAME=$1
PIPELINE_COUNTER=$2
PARENT_IMAGE=$3

TAG=`git describe --abbrev=0 --tags` #get latest tag from git
NEWTAG=b$((${TAG#?}+1)) #Append tag + 1
MESSAGE="Version ${NEWTAG}"
git tag -a -m "${MESSAGE}" "${NEWTAG}"
git push origin "${NEWTAG}"

cd ${SCRIPT_DIR}/docker

docker pull ${PARENT_IMAGE}
docker build -t ${REGISTRY_URL}/${IMAGE_NAME}:${NEWTAG} .
docker tag ${REGISTRY_URL}/${IMAGE_NAME}:${NEWTAG} ${REGISTRY_URL}/${IMAGE_NAME}:latest
docker push ${REGISTRY_URL}/${IMAGE_NAME}
docker rmi ${REGISTRY_URL}/${IMAGE_NAME}:latest ${REGISTRY_URL}/${IMAGE_NAME}:${NEWTAG}
